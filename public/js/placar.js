$('#botaoPlacar').click(togglePlacar);
$('.botao-remover').click(deleteLine);
$('#botaoSync').click(sincronizaPlacar);

function sincronizaPlacar()
{
  var placar = [];
  var linhas = $('tbody>tr');
  linhas.each(function(){
    var usuario = $(this).find('td:nth-child(1)').text();
    var palavra = $(this).find('td:nth-child(2)').text();
    var score = {
      usuario: usuario,
      pontos:  palavra
    };
    placar.push(score);

  });
  var dados = {placar: placar};
  $.post('http://localhost:3000/placar',dados,function(){
    Materialize.toast("As informações foram salvas.", 3000);
    $('.tooltip').tooltipster("close");
    $('#botaoSync').tooltipster("open")
      .tooltipster("content","Sicronização realizada com sucesso!");
  }).fail(function(){
    $('#botaoSync').tooltipster("open")
      .tooltipster("content","Sicronização falhou!");

  }).always(function(){
    setTimeout(function(){
      $('#botaoSync').tooltipster("close")
        .tooltipster('content','Sincroniza o placar com DataBase!');
    }, 2000);
  });
}


function inserePlacar()
{
  var tbody = $('.placar').find('tbody');
  var usuario = $('#usuarios').val();
  console.log(usuario)
  if(!usuario)
    usuario = 'Anônimo';

  var numPalavras = $('#qtdePalavras').text();
  var linha = novaLine(usuario,numPalavras);
  linha.find('.botao-remover').click(deleteLine);
  tbody.append(linha);
  $(".placar").slideDown(500);
}

function novaLine(usuario, palavras)
{
  var linha = $('<tr>');
  var colUsuario = $('<td>').text(usuario);
  var colPalavras = $('<td>').text(palavras);
  var colRemover = $('<td>');
  var link = $('<a>').addClass('botao-remover').attr('href','#');
  var icone = $('<i>').addClass('material-icons').text('delete');
  link.append(icone);
  colRemover.append(link);
  linha.append(colUsuario);
  linha.append(colPalavras);
  linha.append(colRemover);
  return linha;
}

function deleteLine(e)
{
  e.preventDefault();
  // fadeout não remove só esconde
  var linha = $(this).parents('tr');
  console.log(linha)
  linha.fadeOut(600);
  setTimeout(function() {
    linha.remove();
  },600);
}

function togglePlacar()
{
  //$('.placar').toggle();
  $('.placar').stop().slideToggle(600);
  // stop´para todas as ações para executar a última
}

function atualizaPlacar()
{
  $.get('http://localhost:3000/placar',function(data){
    $(data).each(function(){
      var linha = novaLine(this.usuario,this.pontos);
      linha.find('.botao-remover').click(deleteLine);
      $('tbody').append(linha);
    });
  });
}
