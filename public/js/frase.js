$("#botaoTrocaFrase").click(buscaFraseAleatoria);
$('#botaoBuscaFrase').click(buscaFrase);

function buscaFraseAleatoria()
{
	iniciaTroca();
	$.get(
		"http://localhost:3000/frases",trocaFraseAleatoria	)
	.fail(failAcessData)
	.always(function(){
		setTimeout(function(){
			Materialize.toast("Obrigado por usar nosso sistema.", 3000);
		},5000);
	});
}

function trocaFraseAleatoria(data)
{
	var num = Math.floor(Math.random() * data.length);
	trocaFrase(data[num]);
	atualizaComMsg('Frase foi trocada!');
}

function buscaFrase()
{
	iniciaTroca();
	var frase_id = $('#frase_id').val();
	var dados = {id: frase_id};
	$.get("http://localhost:3000/frases",dados,trocaFraseBuscada)
	.fail(failAcessData('Recarregue a página!'));

}

function trocaFraseBuscada(dado)
{
	trocaFrase(dado);
	atualizaComMsg('Frase foi trocada!');
}

function trocaFrase(dado)
{
	$('#frase').text(dado.texto);
	$('#frase').fadeIn();
	$('.spinner').fadeOut();
	$('#tempoRestante').text(dado.tempo);
	$('#tempoRestanteFrase').text(dado.tempo);
}

function atualizaComMsg(message = 'Frase foi trocada!')
{
	atualizaTamanhoFrase();
	Materialize.toast(message, 3000);
}

function atualizaTamanhoFrase()
{
	var frase 		 = $('#frase').text();
	var numPalavras = frase.split(" ").length;
	var numCharac	 = frase.length;
	$('#qtdeCaracterFrase').text(numCharac);
	$('#qtdePalavrasFrase').text(numPalavras);
}

function failAcessData(message, error)
{
	console.log(error)
	$('.erro').fadeIn();
	Materialize.toast(message, 3000);
	setTimeout(function(){
		$(".erro").fadeOut();
		$('.spinner').fadeOut();
	},3000);
}

function iniciaTroca()
{
	$('#frase').fadeOut();
	$('.spinner').fadeIn();
}
