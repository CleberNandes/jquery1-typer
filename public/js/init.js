(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.tooltipped').tooltip();

  }); // end of document ready
})(jQuery); // end of jQuery name space
