
var frase = $('#frase').text();
var arrayPalavras = frase.split(" ");
var qtdePalavras = arrayPalavras.length;
var campo = $('#campoDigitacao');

$(function()
{
  campo.on('input',digitandoJogo);
  campo.on('input',conferePrecisaoDaDigitacao);
  // diferente do click ele pega quando um campo tem focus diferente de quando for clicado
  campo.one('focus',iniciaJogo);
  $('#reiniciaJogo').stop().on('click',reiniciaJogo);
  atualizaTamanhoFrase();
  atualizaPlacar();
  $('#usuarios').selectize({
    create: true,
    sortField: 'text'
  });
});

function conferePrecisaoDaDigitacao()
{
  var digitado = campo.val();
  var comparavel = $('#frase').text().substr(0, digitado.length);
  var esta = (digitado == comparavel)
  campo.toggleClass('digitoCorreto',  esta);
  campo.toggleClass('digitoErrado', ! esta);
}

function digitandoJogo()
{
  var texto = campo.val();
  var arrayPalavras = texto.split(/\S+/);
  var qtdePalavras = arrayPalavras.length - 1;
  var qtdeCaracter = texto.length;
  $('#qtdePalavras').text(qtdePalavras);
  $('#qtdeCaracter').text(qtdeCaracter);
}

function iniciaJogo()
{
    // pego o id do set intervel para passar para o clear parar a contagem
    var tempoRestante = $('#tempoRestante').text();
    var cronometroId = setInterval(function(){
      tempoRestante--;
      if(tempoRestante < 1){
        clearInterval(cronometroId);
        finalizaJogo();
      }
      $('#tempoRestante').text(tempoRestante);
      // usado para parar a contagem quando chegar no zero
    },1000);
}

function reiniciaJogo()
{
    // tira o disable do textarea
    campo.attr("disabled", false);
    campo.toggleClass("fimJogo");
    var tempoFrase = $('#tempoRestanteFrase').text();
    // tira o texto digitado do textarea
    campo.val("");
    $('#qtdePalavras').text("0");
    $('#qtdeCaracter').text("0");
    $('#tempoRestante').text(tempoFrase);
    campo.removeClass('digitoErrado');
    campo.removeClass('digitoCorreto');
    iniciaJogo();
}

function finalizaJogo()
{
  campo.attr('disabled', true);
  campo.toggleClass("fimJogo");
  inserePlacar();
}


// expressão regular para espaços entre palavras
//                  /\S+/


// para colocar um atributo sem valores como o disabled e required
// precisamos passar um segundo argumento
// $('#campoDigitacao').attr('disabled', true);
// se quiser tirar um atributo desse passar o segundo argumento como false

// a função one que vai no mesmo lugar da função on serve apenas uma vez


// a função $(document).ready(function(){}) da na mesma de
// $(function(){})
